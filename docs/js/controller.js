$(function () {

	//DROPDOWN MENU HOVER
	$(".dropdown.hover").hover(
		function() {
		$(this).addClass("aktif");
		},
		function() {
		$(this).removeClass("aktif");
		return false;	
	});

	//DROPDOWN MENU CLICK
	$(".dropdown.click").click(function(event){
		$(this).toggleClass("aktif");
		return false;
	});

	$(".datepick").datepick({
		autoSize: true
	});

	$('.tip').tipr();

	//IMAGE LIQUID
    $(".lqd").imgLiquid();

    //SISIP VIDEO RESPONSIVE 16x9
    $(".sisip_video iframe").wrap("<div class='ratiobox ratio_16_9'><div class='ratiobox_content'></div></div>");

	/*TABBING*/
	$(".tabs").idTabs(function(id,list,set){
		$("a",set).removeClass("selected")
		.filter("[href='"+id+"']",set).addClass("selected");
		for(i in list)
		$(list[i]).hide();
		$(id).fadeIn();
		return false;
	}); 

	/*TABBING CLICKABLE LINKS*/
	(function($) {
	    $.QueryString = (function(a) {
	        if (a == "") return {};
	            var b = {};
	            for (var i = 0; i < a.length; ++i) {
	                var p=a[i].split('=');
	                if (p.length != 2) continue;
	                    b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
	                }
	                return b;
	    })(window.location.search.substr(1).split('&'))
	})(jQuery);

	$("#" + $.QueryString["tabLink"]).click();

	//STICKY
	$(window).load(function(){

		return $("[data-sticky_column]").stick_in_parent({
		  parent: "[data-sticky_parent]",
		  offset_top: 0,
		  inner_scrolling: false
		})
		.on('sticky_kit:bottom', function(e) {
			$(this).parent().css('position', 'static');
		})
		.on('sticky_kit:unbottom', function(e) {
			$(this).parent().css('position', 'relative');
		});

		$(window).resize(function() {
			$(document.body).trigger("sticky_kit:recalc");
		});
				
		$('.list.feed .more_feed').click(function() {
			$(document.body).trigger("sticky_kit:recalc");
		});

	});

});

