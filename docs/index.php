<!DOCTYPE html>
<html lang="en">
    
    <?php include "includes/head.php"; ?>

    <body>

        <header>
            FRAMEWORK
        </header>

            <div class="container_full">

                <div class="grid_row">
                    
                    <div id="menu" class="col_sm_3 col_md_2">

                        <div class="pd15">
                            CSS
                        </div>
                        
                        <ul>
                            <li>
                                <a href="#typo" class="active">Typography</a>
                            </li>
                            <li>
                                <a href="#grid">Grid System</a>
                            </li>
                            <li>
                                <a href="#table">Tables</a>
                            </li>
                            <li>
                                <a href="#form">Forms</a>
                            </li>
                            <li>
                                <a href="#helper">Helper Class</a>
                            </li>
                        </ul>

                        <hr>

                        <div class="pd15">
                            Components
                        </div>
                        
                        <ul>
                            <li>
                                <a href="#button">Buttons</a>
                            </li>
                            <li>
                                <a href="#pagination">Pagination</a>
                            </li>
                            <li>
                                <a href="#list">List Rows & Columns</a>
                            </li>
                        </ul>

                    </div>

                    <div id="content" class="col_sm_9 col_md_10">
                        
                        <div class="contoh">

                            <h3 class="mb10">This Is System</h3>

                            <p>Ini adalah sebuah sistem, framework css/sass, rangka kerja, yang berkaitan satu sama lain. Dalam penggunaan nya sudah diminimalisir seminimal mungkin
                            secara umum, namun tetap kaya akan kebutuhan styling layout.</p>

                            <!-- TYPE -->
                            <div id="typo" class="section">
                            
                                <h3 class="mb10">Typography</h3>

                                <h4 class="mb10">Heading</h4>

                                <div class="box highlight pd15 mb20">
                                    <h1> &lt;h1&gt; Heading</h1>
                                    <h2> &lt;h2&gt; Heading</h2>
                                    <h3> &lt;h3&gt; Heading</h3>
                                    <h4> &lt;h4&gt; Heading</h4>
                                    <h5> &lt;h5&gt; Heading</h5>
                                    <h6> &lt;h6&gt; Heading</h6>
                                </div>

                                <h4 class="mb10">Text Align & Bold</h4>

                                <div class="box highlight pd15 mb20">
                                
                                    <p class="text_left">Text Left</p>
                                    <p class="text_center">Text Center</p>
                                    <p class="text_right">Text Right</p>
                                    <p class="text_justify">
                                        Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula.

    Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ullamcorper nulla non metus auctor fringilla. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec ullamcorper nulla non metus auctor fringilla.
                                    </p>
                                    <p class="bold">Text Bold</p>

                                </div>

                                <figure class="mb20">

                                    <pre><code class="language-html" data-lang="html"><span class="nt">&lt;p</span> <span class="na">class=</span><span class="s">"text_left"</span><span class="nt">&gt;</span>Text Left<span class="nt">&lt;/p&gt;</span>
    <span class="nt">&lt;p</span> <span class="na">class=</span><span class="s">"text_center"</span><span class="nt">&gt;</span>Text Center<span class="nt">&lt;/p&gt;</span>
    <span class="nt">&lt;p</span> <span class="na">class=</span><span class="s">"text_right"</span><span class="nt">&gt;</span>Text Right<span class="nt">&lt;/p&gt;</span>
    <span class="nt">&lt;p</span> <span class="na">class=</span><span class="s">"text_justify"</span><span class="nt">&gt;</span>Text Justify<span class="nt">&lt;/p&gt;</span>
    <span class="nt">&lt;p</span> <span class="na">class=</span><span class="s">"text_nowrap"</span><span class="nt">&gt;</span>Text No Wrap<span class="nt">&lt;/p&gt;</span>
    <span class="nt">&lt;p</span> <span class="na">class=</span><span class="s">"bold"</span><span class="nt">&gt;</span>Text Bold<span class="nt">&lt;/p&gt;</span></code></pre>

                                </figure>

                                <h4 class="mb10">Lists</h4>

                                <div class="box highlight pd15 mb20">

                                    <h5 class="mb10">Unordered</h5> <hr>

                                    <ul>
                                        <li>Lorem ipsum dolor sit amet</li>
                                        <li>Consectetur adipiscing elit</li>
                                        <li>Integer molestie lorem at massa</li>
                                        <li>Facilisis in pretium nisl aliquet</li>
                                        <li>
                                        Nulla volutpat aliquam velit
                                            <ul>
                                                <li>Phasellus iaculis neque</li>
                                                <li>Purus sodales ultricies</li>
                                                <li>Vestibulum laoreet porttitor sem</li>
                                                <li>Ac tristique libero volutpat at</li>
                                            </ul>
                                        </li>
                                        <li>Faucibus porta lacus fringilla vel</li>
                                        <li>Aenean sit amet erat nunc</li>
                                        <li>Eget porttitor lorem</li>
                                    </ul>

                                    <hr>

                                    <h5 class="mb10">Ordered</h5> <hr>

                                    <ol>
                                        <li>Lorem ipsum dolor sit amet</li>
                                        <li>Consectetur adipiscing elit</li>
                                        <li>Integer molestie lorem at massa</li>
                                        <li>Facilisis in pretium nisl aliquet</li>
                                        <li>
                                        Nulla volutpat aliquam velit
                                            <ol>
                                                <li>Phasellus iaculis neque</li>
                                                <li>Purus sodales ultricies</li>
                                                <li>Vestibulum laoreet porttitor sem</li>
                                                <li>Ac tristique libero volutpat at</li>
                                            </ol>
                                        </li>
                                        <li>Faucibus porta lacus fringilla vel</li>
                                        <li>Aenean sit amet erat nunc</li>
                                        <li>Eget porttitor lorem</li>
                                    </ol>

                                </div>

                                <figure class="mb20">
                                    <pre>
    <code class="language-html" data-lang="html"><span class="nt">&lt;ul</span><span class="nt">&gt;</span>
        <span class="nt">&lt;li</span><span class="nt">&gt;</span>...<span class="nt">&lt;/li&gt;</span>
    <span class="nt">&lt;/ul&gt;</span>
    </code></pre>

                                </figure>

                            </div>

                            <!-- GRID -->
                            <div id="grid" class="section">

                                <h3 class="mb10">Grid System</h3>

                                <p>Grid System terdiri dari 12 kolom responsive dengan konsep <i><strong>mobile first</strong></i> yang disesuaikan berdasarkan lebar viewport device. Didefinisikan dengan penamaan class sesuai nama pilihan layout</p>
                                <p>Grid system ini untuk memudahkan dalam pembuatan layout dengan beberapa kolom, berikut ini cara penggunaan nya:</p>

                                <ul>
                                    <li>Dibungkus dengan class <i class="bold">.grid_row</i></li>
                                    <li>Kemudian di dalam nya kolom-kolom dengan penamaan sesuai lebar kolom yg diinginkan, dengan spesifikasi nomor 1 sampai 12 yang terdefinisi berdasarkan 12 kolom. Sebagai contoh, jika 3 kolom dengan lebar sama, maka class dari ketiga kolom yang digunakan adalah <i class="bold">.col_4</i></li>
                                    <li>Class grid yang berlaku adalah untuk viewport dengan lebar device yang lebih besar dari atau sama dengan ukuran breakpoint, dan menimpa class yang ditargetkan pada ukuran device yang lebih kecil. Oleh karena itu, misalnya menerapkan setiap <i class="bold">.col_*</i> untuk elemen, tidak hanya akan mempengaruhi styling pada perangkat mobile, tetapi juga pada perangkat besar jika kelas <i class="bold">.col_md_*</i> atau class <i class="bold">.col_*</i> tidak didefinisikan.</li>
                                    <li>Oleh karena itu, untuk website yang tidak responsive, maka untuk kolom gunakan saja class <i class="bold">.col_*</i></li>
                                    <li>Untuk membuat kolom-kolom tanpa <i class="bold">gap</i> (jarak antar kolom), ditambahkan class <i class="bold">no_gap</i> pada class <i class="bold">grid_row</i></li>
                                    <li>Jarak antar kolom di sini 15px, bisa diatur di file <span class="bold">scss/assets/_variables.scss</span></li>
                                </ul>

                                <h4 class="mb10">Grid Options</h4>

                                <table class="table">
                                    <tr>
                                        <th>&nbsp;</th>
                                        <th>Extra small devices <br> <small>Phones (<576px)</small></th>
                                        <th>Small devices<br> <small>Landscape phones (≥576px)</small></th>
                                        <th>Medium devices <br> <small>Tablets (≥768px)</small></th>
                                        <th>Large devices <br> <small>Desktops (≥992px)</small></th>
                                    </tr>
                                    <tr>
                                        <td class="bold">Grid Behaviour</td>
                                        <td>Horizontal at all times</td>
                                        <td colspan="3">Collapsed to start, horizontal above breakpoints</td>
                                    </tr>
                                    <tr>
                                        <td class="bold">Class prefix</td>
                                        <td>.col_*</td>
                                        <td>.col_sm_*</td>
                                        <td>.col_md_*</td>
                                        <td>.col_lg_*</td>
                                    </tr>
                                    <tr>
                                        <td class="bold"># of columns</td>
                                        <td colspan="4">12</td>
                                    </tr>
                                </table>

                                <br>

                                <div class="box highlight pd15 mb20">

                                    <h4 class="mb10">Non-responsive</h4>

                                    <div class="grid_row contoh">
                                        <div class="col_1">
                                            <div class="pd10">.col_1</div>
                                        </div>
                                        <div class="col_1">
                                            <div class="pd10">.col_1</div>
                                        </div>
                                        <div class="col_1">
                                            <div class="pd10">.col_1</div>
                                        </div>
                                        <div class="col_1">
                                            <div class="pd10">.col_1</div>
                                        </div>
                                        <div class="col_1">
                                            <div class="pd10">.col_1</div>
                                        </div>
                                        <div class="col_1">
                                            <div class="pd10">.col_1</div>
                                        </div>
                                        <div class="col_1">
                                            <div class="pd10">.col_1</div>
                                        </div>
                                        <div class="col_1">
                                            <div class="pd10">.col_1</div>
                                        </div>
                                        <div class="col_1">
                                            <div class="pd10">.col_1</div>
                                        </div>
                                        <div class="col_1">
                                            <div class="pd10">.col_1</div>
                                        </div>
                                        <div class="col_1">
                                            <div class="pd10">.col_1</div>
                                        </div>
                                        <div class="col_1">
                                            <div class="pd10">.col_1</div>
                                        </div>
                                    </div>

                                    <br>

                                    <div class="grid_row contoh">
                                        <div class="col_7">
                                            <div class="pd10">.col_7</div>
                                        </div>
                                        <div class="col_5">
                                            <div class="pd10">.col_5</div>
                                        </div>
                                    </div>

                                    <br>

                                    <div class="grid_row contoh">
                                        <div class="col_4">
                                            <div class="pd10">.col_4</div>
                                        </div>
                                        <div class="col_4">
                                            <div class="pd10">.col_4</div>
                                        </div>
                                        <div class="col_4">
                                            <div class="pd10">.col_4</div>
                                        </div>
                                    </div>

                                    <br><br>

                                    <h4 class="mb10">Responsive</h4>

                                    <div class="grid_row contoh">
                                        <div class="col_md_6 col_lg_3">
                                            <div class="pd10">.col_md_6 .col_lg_3</div>
                                        </div>
                                        <div class="col_md_6 col_lg_4">
                                            <div class="pd10">.col_md_6 .col_lg_4</div>
                                        </div>
                                        <div class="col_md_12 col_lg_5">
                                            <div class="pd10">.col_md_12 .col_lg_5</div>
                                        </div>
                                    </div>

                                    <br>

                                    <div class="grid_row contoh">
                                        <div class="col_2 col_md_4 col_lg_3">
                                            <div class="pd10">.col_2 .col_md_4 .col_lg_3</div>
                                        </div>
                                        <div class="col_10 col_md_8 col_lg_4">
                                            <div class="pd10">.col_10 .col_md_8 .col_lg_4</div>
                                        </div>
                                        <div class="col_12 col_md_12 col_lg_5">
                                            <div class="pd10">.col_12 .col_md_12 .col_lg_5</div>
                                        </div>
                                    </div>

                                    <br><br>

                                    <h4 class="mb10">No Gap</h4>

                                    <div class="grid_row no_gap contoh">
                                        <div class="col_4">
                                            <div class="pd10">.col_4</div>
                                        </div>
                                        <div class="col_4">
                                            <div class="pd10">.col_4</div>
                                        </div>
                                        <div class="col_4">
                                            <div class="pd10">.col_4</div>
                                        </div>
                                    </div>

                                    <div class="grid_row no_gap contoh">
                                        <div class="col_2">
                                            <div class="pd10">.col_2</div>
                                        </div>
                                        <div class="col_4">
                                            <div class="pd10">.col_4</div>
                                        </div>
                                        <div class="col_6">
                                            <div class="pd10">.col_6</div>
                                        </div>
                                    </div>

                                    <br><br>

                                    <h4 class="mb10">With equal width by col items</h4>
                                    Untuk kolom-kolom yang lebar nya sama semua, dan menyesuaikan lebar container<br><br>

                                    <div class="grid_row contoh">
                                        <div class="col">
                                            <h4>Judul</h4>Summary
                                        </div>
                                        <div class="col">
                                            <h4>Judul</h4>Summary
                                        </div>
                                        <div class="col">
                                            <h4>Judul</h4>Summary
                                        </div>
                                        <div class="col">
                                            <h4>Judul</h4>Summary
                                        </div>
                                        <div class="col">
                                            <h4>Judul</h4>Summary
                                        </div>
                                    </div>
                                    
                                </div>

                                <figure class="mb20">

                                <h4 class="mb10">Non-responsive ~> Gunakan class .col_*</h4>

    <pre>
    <code class="language-html" data-lang="html">
    <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_row"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col_1"</span><span class="nt">&gt;</span>Konten<span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col_1"</span><span class="nt">&gt;</span>Konten<span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col_1"</span><span class="nt">&gt;</span>Konten<span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col_1"</span><span class="nt">&gt;</span>Konten<span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col_1"</span><span class="nt">&gt;</span>Konten<span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col_1"</span><span class="nt">&gt;</span>Konten<span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col_1"</span><span class="nt">&gt;</span>Konten<span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col_1"</span><span class="nt">&gt;</span>Konten<span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col_1"</span><span class="nt">&gt;</span>Konten<span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col_1"</span><span class="nt">&gt;</span>Konten<span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col_1"</span><span class="nt">&gt;</span>Konten<span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col_1"</span><span class="nt">&gt;</span>Konten<span class="nt">&lt;/div&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
    </code>
    </pre>

    <br>

    <h4 class="mb10">No Gap</h4>

    <pre>
    <code class="language-html" data-lang="html">
    <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_row no_gap"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col_4"</span><span class="nt">&gt;</span>Konten<span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col_4"</span><span class="nt">&gt;</span>Konten<span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col_4"</span><span class="nt">&gt;</span>Konten<span class="nt">&lt;/div&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
    </code>
    </pre>

    <br>

    <!-- <h4 class="mb10">With Inline</h4>

    <pre>
    <code class="language-html" data-lang="html">
    <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_row gap inline"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col_4"</span><span class="nt">&gt;</span>Konten<span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col_4"</span><span class="nt">&gt;</span>Konten<span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col_4"</span><span class="nt">&gt;</span>Konten<span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col_4"</span><span class="nt">&gt;</span>Konten<span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col_4"</span><span class="nt">&gt;</span>Konten<span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col_4"</span><span class="nt">&gt;</span>Konten<span class="nt">&lt;/div&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
    </code></pre> -->

                                </figure>

                            </div>

                            <!-- TABLES -->
                            <div id="table" class="section">

                                <h3 class="mb10">Tables</h3>

                                <p>Untuk styling table, gunakan class <i class="bold">.table</i></p>

                                <div class="box highlight pd15 mb20">

                                    <h4 class="mb10">Table Default</h4>

                                    <table class="table">
                                        <tr>
                                            <th width="45" class="text_right"></th>
                                            <th>Kolom 1</th>
                                            <th>Kolom 2</th>
                                        </tr>
                                        <tr>
                                            <td class="text_right">1.</td>
                                            <td>Data</td>
                                            <td>Data</td>
                                        </tr>
                                        <tr>
                                            <td class="text_right">2.</td>
                                            <td>Data</td>
                                            <td>Data</td>
                                        </tr>
                                        <tr>
                                            <td class="text_right">3.</td>
                                            <td>Data</td>
                                            <td>Data</td>
                                        </tr>
                                    </table>

                                    <br>

                                    <h4 class="mb10">Table Dark</h4>

                                    <table class="table_dark">
                                        <tr>
                                            <th width="45" class="text_right"></th>
                                            <th>Kolom 1</th>
                                            <th>Kolom 2</th>
                                        </tr>
                                        <tr>
                                            <td class="text_right">1.</td>
                                            <td>Data</td>
                                            <td>Data</td>
                                        </tr>
                                        <tr>
                                            <td class="text_right">2.</td>
                                            <td>Data</td>
                                            <td>Data</td>
                                        </tr>
                                        <tr>
                                            <td class="text_right">3.</td>
                                            <td>Data</td>
                                            <td>Data</td>
                                        </tr>
                                    </table>

                                </div>

                                <figure class="mb20">

    <pre>
    <code class="language-html" data-lang="html"><span class="nt">&lt;table</span> <span class="na">class=</span><span class="s">"table"</span><span class="nt">&gt;</span>
      ...
    <span class="nt">&lt;/table&gt;</span>
    </code></pre>

    <hr>

    <pre>
    <code class="language-html" data-lang="html"><span class="nt">&lt;table</span> <span class="na">class=</span><span class="s">"table_small"</span><span class="nt">&gt;</span>
      ...
    <span class="nt">&lt;/table&gt;</span>
    </code></pre>

    <hr>

    <pre>
    <code class="language-html" data-lang="html"><span class="nt">&lt;table</span> <span class="na">class=</span><span class="s">"table_dark"</span><span class="nt">&gt;</span>
      ...
    <span class="nt">&lt;/table&gt;</span>
    </code></pre>
                                    
                                </figure>

                            </div>

                            <!-- FORMS -->
                            <div id="form" class="section">

                                <h3 class="mb10">Forms</h3>

                                <p>Untuk styling form, gunakan class <i class="bold">.form</i></p>

                                <div class="box highlight pd15 mb20">

                                    <h4 class="mb10">Form Default</h4> <hr>

                                    <form action="#" class="form">
                                        <label for="label1">Username</label>
                                        <input type="text" class="text" id="label1">
                                        <label for="label2">Password</label>
                                        <input type="password" class="text" id="label2">
                                        <input type="submit" class="btn mt10" value="LOGIN">
                                    </form>

                                    <br><br>

                                    <h4 class="mb10">Form Inline ~> Gunakan class helper</h4> <hr>

                                    <form action="#" class="form">
                                        <label for="label12" class="fl mr10">Username</label>
                                        <input type="text" class="text fl" id="label12">
                                        <label for="label22" class="fl mr10 ml20">Password</label>
                                        <input type="password" class="text fl" id="label22">
                                        <input type="submit" class="btn fl ml10" value="LOGIN">
                                        <div class="clearfix"></div>
                                    </form>

                                    <br><br>

                                    <h4 class="mb10">Form Horizontal ~> Gunakan grid system</h4> <hr>

                                    <form action="#" class="form">
                                        <div class="grid_row">
                                            <label for="label13" class="col_md_2">Username</label>
                                            <div class="col_md_10">
                                                <input type="text" class="text half" id="label13">
                                            </div>
                                        </div>
                                        <div class="grid_row">
                                            <label for="label32" class="col_md_2">Password</label>
                                            <div class="col_md_10">
                                                <input type="password" class="text half" id="label32">
                                            </div>
                                        </div>
                                        <div class="grid_row">
                                            <label for="select1" class="col_md_2">Type</label>
                                            <div class="col_md_10">
                                                <div class="styled-select half mb10">
                                                    <div class="caret"></div>
                                                    <select id="select1">
                                                        <option value="0">- Choose -</option>
                                                        <option value="1">Pilihan 1</option>
                                                        <option value="2">Pilihan 2</option>
                                                        <option value="3">Pilihan 3</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="grid_row">
                                            <div class="col_md_2">&nbsp;</div>
                                            <div class="col_md_10">
                                                <div class="checkbox">
                                                    <label for="check1">
                                                        <input type="checkbox" id="check1">
                                                        Checkbox
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="grid_row">
                                            <div class="col_md_2">&nbsp;</div>
                                            <div class="col_md_10">
                                                <input type="submit" class="btn mt10" value="LOGIN">
                                            </div>
                                        </div>
                                    </form>

                                </div>

                                <figure class="mb20">

    <pre>
    <code class="language-html" data-lang="html"><span class="nt">&lt;form</span> <span class="na">class=</span><span class="s">"form"</span><span class="nt">&gt;</span>
    ...
    <span class="nt">&lt;/form&gt;</span>
    </code>
    </pre>

    <br>

    <h4 class="mb10">Form Horizontal</h4>

    <pre>
    <code class="language-html" data-lang="html">
    <span class="nt">&lt;form</span> <span class="na">class=</span><span class="s">"form"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_row"</span><span class="nt">&gt;</span>
            <span class="nt">&lt;label</span> <span class="na">class=</span><span class="s">"col_md_2"</span><span class="nt">&gt;</span>Username<span class="nt">&lt;/label&gt;</span>
            <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col_md_10"</span><span class="nt">&gt;</span>
                <span class="nt">&lt;input</span> <span class="na">type=</span><span class="s">"text"</span> <span class="na">class=</span><span class="s">"text half"</span><span class="nt">&gt;</span>
            <span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_row"</span><span class="nt">&gt;</span>
            <span class="nt">&lt;label</span> <span class="na">class=</span><span class="s">"col_md_2"</span><span class="nt">&gt;</span>Password<span class="nt">&lt;/label&gt;</span>
            <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col_md_10"</span><span class="nt">&gt;</span>
                <span class="nt">&lt;input</span> <span class="na">type=</span><span class="s">"password"</span> <span class="na">class=</span><span class="s">"text half"</span><span class="nt">&gt;</span>
            <span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_row"</span><span class="nt">&gt;</span>
            <span class="nt">&lt;label</span> <span class="na">class=</span><span class="s">"col_md_2"</span><span class="nt">&gt;</span>Type<span class="nt">&lt;/label&gt;</span>
            <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col_md_10"</span><span class="nt">&gt;</span>
                <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"styled-select half mb5"</span><span class="nt">&gt;</span>
                    <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"caret"</span><span class="nt">&gt;</span><span class="nt">&lt;/div&gt;</span>
                    <span class="nt">&lt;select&gt;</span>
                        <span class="nt">&lt;option</span> <span class="na">value=</span><span class="s">"0"</span><span class="nt">&gt;</span>- Choose -<span class="nt">&lt;/option&gt;</span>
                        <span class="nt">&lt;option</span> <span class="na">value=</span><span class="s">"1"</span><span class="nt">&gt;</span>Pilihan 1<span class="nt">&lt;/option&gt;</span>
                        <span class="nt">&lt;option</span> <span class="na">value=</span><span class="s">"2"</span><span class="nt">&gt;</span>Pilihan 2<span class="nt">&lt;/option&gt;</span>
                    <span class="nt">&lt;/select&gt;</span>
                <span class="nt">&lt;/div&gt;</span>
            <span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_row"</span><span class="nt">&gt;</span>
            <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col_md_2"</span><span class="nt">&gt;</span>&nbsp;<span class="nt">&lt;/div&gt;</span>
            <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col_md_10"</span><span class="nt">&gt;</span>
                <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"checkbox"</span><span class="nt">&gt;</span>
                    <span class="nt">&lt;label</span><span class="nt">&gt;</span>
                        <span class="nt">&lt;input</span> <span class="na">type=</span><span class="s">"checkbox"</span><span class="nt">&gt;</span> Checkbox
                    <span class="nt">&lt;/label&gt;</span>
                <span class="nt">&lt;/div&gt;</span>
            <span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_row"</span><span class="nt">&gt;</span>
            <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col_md_2"</span><span class="nt">&gt;</span>&nbsp;<span class="nt">&lt;/div&gt;</span>
            <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col_md_10"</span><span class="nt">&gt;</span>
                <span class="nt">&lt;input</span> <span class="na">type=</span><span class="s">"submit"</span> <span class="na">class=</span><span class="s">"btn mt10"</span> <span class="na">value=</span><span class="s">"LOGIN"</span><span class="nt">&gt;</span>
            <span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;/div&gt;</span>
    <span class="nt">&lt;/form&gt;</span>
    </code>
    </pre>

                                </figure>

                            </div>

                            <!-- HELPERS -->
                            <div id="helper" class="section">

                                <h3 class="mb10">Helpers</h3>

                                <p>Untuk style penunjang</p>
                                
                                <div class="box highlight pd15 mb20">

                                    <p class="relative">Position Relative</p>
                                    <a href="#" class="block">Display Block</a>

                                    <hr>

                                    <p>Circle</p>

                                    <span class="ratiobox box_img">
                                        <span class="icon_inside">icon</span>
                                        <span class="ratiobox_content circle lqd">
                                            <img src="images/img1.jpg" alt="img" class="circle">
                                        </span>
                                    </span>
                                
                                </div>

                                <figure class="mb20">

                                    <pre>
    <code class="language-html" data-lang="html"><span class="nt">&lt;p</span> <span class="na">class=</span><span class="s">"relative"</span><span class="nt">&gt;</span>Position Relative<span class="nt">&lt;/p&gt;</span>
    <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"block"</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>Display Block<span class="nt">&lt;/a&gt;</span>

    <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"ratiobox box_img"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"ratiobox_content circle lqd"</span><span class="nt">&gt;</span>
            <span class="nt">&lt;img</span> <span class="na">src=</span><span class="s">"images/img1.jpg"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;/span&gt;</span>
    <span class="nt">&lt;/span&gt;</span>
    </code></pre>

                                </figure>

                            </div>

                            <!-- BUTTONS -->
                            <div id="button" class="section">

                                <h3 class="mb10">Buttons</h3>

                                <p>Untuk styling buttun, gunakan class <i class="bold">.btn</i></p>

                                <div class="box highlight pd15 mb20">

                                    <a href="#" class="btn">Link</a>
                                    <button class="btn">Button</button>
                                    <input type="button" class="btn" value="Input">
                                    <input type="submit" class="btn" value="Submit">

                                </div>

                                <figure class="mb20">

    <pre><code class="language-html" data-lang="html"><span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"btn"</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>Link<span class="nt">&lt;/a&gt;</span>
    <span class="nt">&lt;button</span> <span class="na">class=</span><span class="s">"btn"</span> <span class="na">type=</span><span class="s">"submit"</span><span class="nt">&gt;</span>Button<span class="nt">&lt;/button&gt;</span>
    <span class="nt">&lt;input</span> <span class="na">class=</span><span class="s">"btn"</span> <span class="na">type=</span><span class="s">"button"</span> <span class="na">value=</span><span class="s">"Input"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;input</span> <span class="na">class=</span><span class="s">"btn"</span> <span class="na">type=</span><span class="s">"submit"</span> <span class="na">value=</span><span class="s">"Submit"</span><span class="nt">&gt;</span>
    </code></pre>
                                </figure>

                                <h4 class="mb10">Styling</h4>

                                <div class="box highlight pd15 mb20">

                                    <a href="#" class="btn btn_nofill">Link</a>
                                    <a href="#" class="btn btn_blue">Link</a>
                                    <a href="#" class="btn btn_red">Link</a>
                                    <a href="#" class="btn btn_orange">Link</a>
                                    <a href="#" class="btn btn_green">Link</a>
                                    <a href="#" class="btn btn_grey">Link</a>

                                </div>

                                <figure class="mb20">

    <pre><code class="language-html" data-lang="html"><span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"btn btn_nofill"</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>Link<span class="nt">&lt;/a&gt;</span>
    <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"btn btn_blue"</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>Link<span class="nt">&lt;/a&gt;</span>
    <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"btn btn_red"</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>Link<span class="nt">&lt;/a&gt;</span>
    <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"btn btn_orange"</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>Link<span class="nt">&lt;/a&gt;</span>
    <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"btn btn_green"</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>Link<span class="nt">&lt;/a&gt;</span>
    <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"btn btn_grey"</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>Link<span class="nt">&lt;/a&gt;</span>
    </code></pre>
                                </figure>

                                <h4 class="mb10">Sizes</h4>

                                <div class="box highlight pd15 mb20">

                                    <a href="#" class="btn btn_small">Link</a>
                                    <a href="#" class="btn">Link</a>
                                    <a href="#" class="btn btn_large">Link</a>

                                </div>

                                <figure class="mb20">

    <pre><code class="language-html" data-lang="html"><span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"btn btn_small"</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>Link<span class="nt">&lt;/a&gt;</span>
    <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"btn"</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>Link<span class="nt">&lt;/a&gt;</span>
    <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"btn btn_large"</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>Link<span class="nt">&lt;/a&gt;</span>
    </code></pre>
                                </figure>

                                <h4 class="mb10">Button Group</h4>

                                <div class="box highlight pd15 mb20">

                                    <h4 class="mb10">Default</h4>

                                    <hr>

                                    <div class="btn_group">
                                        <a href="#" class="btn"><span>Link</span></a>
                                        <a href="#" class="btn"><span>Link</span></a>
                                        <a href="#" class="btn"><span>Link</span></a>
                                    </div>

                                    <div class="btn_group">
                                        <a href="#" class="btn btn_nofill"><span>Link</span></a>
                                        <a href="#" class="btn btn_nofill"><span>Link</span></a>
                                        <a href="#" class="btn btn_nofill"><span>Link</span></a>
                                    </div>

                                    <br><br>

                                    <h4 class="mb10">Justified Align</h4>

                                    <hr>

                                    <div class="btn_group btn_group_justified">
                                        <a href="#" class="btn"><span>Link</span></a>
                                        <a href="#" class="btn"><span>Link</span></a>
                                        <a href="#" class="btn"><span>Link</span></a>
                                    </div>

                                    <br><br>

                                    <div class="btn_group btn_group_justified">
                                        <a href="#" class="btn btn_nofill"><span>Link</span></a>
                                        <a href="#" class="btn btn_nofill"><span>Link</span></a>
                                        <a href="#" class="btn btn_nofill"><span>Link</span></a>
                                    </div>

                                </div>

                                <figure class="mb20">

    <pre>
    <code class="language-html" data-lang="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"btn_group"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"btn"</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span><span class="nt">&lt;span&gt;</span>Link<span class="nt">&lt;/span&gt;</span><span class="nt">&lt;/a&gt;</span>
        <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"btn"</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span><span class="nt">&lt;span&gt;</span>Link<span class="nt">&lt;/span&gt;</span><span class="nt">&lt;/a&gt;</span>
        <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"btn"</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span><span class="nt">&lt;span&gt;</span>Link<span class="nt">&lt;/span&gt;</span><span class="nt">&lt;/a&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
    </code></pre>

    <hr>

    <pre>
    <code class="language-html" data-lang="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"btn_group btn_group_justified"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"btn"</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span><span class="nt">&lt;span&gt;</span>Link<span class="nt">&lt;/span&gt;</span><span class="nt">&lt;/a&gt;</span>
        <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"btn"</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span><span class="nt">&lt;span&gt;</span>Link<span class="nt">&lt;/span&gt;</span><span class="nt">&lt;/a&gt;</span>
        <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"btn"</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span><span class="nt">&lt;span&gt;</span>Link<span class="nt">&lt;/span&gt;</span><span class="nt">&lt;/a&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
    </code></pre>


                                </figure>

                            </div>

                            <!-- PAGINATION -->
                            <div id="pagination" class="section">

                                <h3 class="mb10">Pagination</h3>

                                <div class="box highlight pd15 mb20">

                                    <div class="paging">
                                        <a href="#">&laquo;</a>
                                        <a href="#" class="selected">1</a>
                                        <a href="#">2</a>
                                        <a href="#">3</a>
                                        <a href="#">4</a>
                                        <a href="#">5</a>
                                        <a href="#">&raquo;</a>
                                    </div>

                                    <hr>

                                    <div class="paging text_center">
                                        <a href="#">&laquo;</a>
                                        <a href="#" class="selected">1</a>
                                        <a href="#">2</a>
                                        <a href="#">3</a>
                                        <a href="#">4</a>
                                        <a href="#">5</a>
                                        <a href="#">&raquo;</a>
                                    </div>

                                    <hr>

                                    <div class="paging text_right">
                                        <a href="#">&laquo;</a>
                                        <a href="#" class="selected">1</a>
                                        <a href="#">2</a>
                                        <a href="#">3</a>
                                        <a href="#">4</a>
                                        <a href="#">5</a>
                                        <a href="#">&raquo;</a>
                                    </div>

                                </div>

                                <figure class="mb20">
                                    <pre>
    <code class="language-html" data-lang="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"paging"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>&amp;laquo;<span class="nt">&lt;/a&gt;</span>
        <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>1<span class="nt">&lt;/a&gt;</span>
        <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>2<span class="nt">&lt;/a&gt;</span>
        <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>3<span class="nt">&lt;/a&gt;</span>
        <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>4<span class="nt">&lt;/a&gt;</span>
        <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>5<span class="nt">&lt;/a&gt;</span>
        <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>&amp;raquo;<span class="nt">&lt;/a&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
    </code></pre>

                                </figure>

                            </div>

                            <!-- PAGINATION -->
                            <div id="list" class="section">

                                <h3 class="mb10">List</h3>

                                <h4 class="mb10">Simple / Menu Item</h4>

                                <div class="box highlight pd15 mb20">

                                    <div class="list">
                                        <a href="#">Download</a>
                                        <a href="#">What's included</a>
                                        <a href="#">Compiling CSS and JavaScript</a>
                                        <a href="#">Basic Template</a>
                                    </div>

                                    <br><br>

                                    <h4>With &lt;article&gt; & contextual class title</h4>

                                    <hr>

                                    <div class="list">
                                        <article>
                                            <span class="f12">Senin 16 Jan 2017, 11:58 WIB</span>
                                            <h3 class="title"><a href="#">Saat Nurdin Halid Kesal dan Menantang Ical yang Penakut</a></h3>
                                        </article>
                                        <article>
                                            <span class="f12">Senin 16 Jan 2017, 11:58 WIB</span>
                                            <h3 class="title"><a href="#">Protes Kenaikan BBM, Elite PDIP Ini Sebut Jokowi Presiden Prematur</a></h3>
                                        </article>
                                        <article>
                                            <span class="f12">Senin 16 Jan 2017, 11:58 WIB</span>
                                            <h3 class="title"><a href="#">Asus Zenfone 2 RAM 4 GB Masuk Indonesia, Harganya?</a></h3>
                                        </article>
                                    </div>

                                </div>

                                <figure class="mb20">
                                    <pre>
    <code class="language-html" data-lang="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"list"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>Download<span class="nt">&lt;/a&gt;</span>
        <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>What's included<span class="nt">&lt;/a&gt;</span>
        <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>Compiling CSS and JavaScript<span class="nt">&lt;/a&gt;</span>
        <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>Basic Template<span class="nt">&lt;/a&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
    </code></pre>

                                </figure>

                                <h4 class="mb10">Media Rows</h4>

                                <div class="box highlight pd15 mb20">

                                    <div class="list media_rows">
                                        <article>
                                            <a href="#">
                                                <span class="ratiobox box_img">
                                                    <span class="ratiobox_content lqd">
                                                        <img src="images/img1.jpg" alt="">
                                                    </span>
                                                </span>
                                                <span class="box_text">
                                                    <h2 class="title">Saat Nurdin Halid Kesal dan Menantang Ical yang Penakut</h2>
                                                </span>
                                            </a>
                                        </article>
                                        <article>
                                            <a href="#">
                                                <span class="ratiobox box_img">
                                                    <span class="ratiobox_content lqd">
                                                        <img src="images/img1.jpg" alt="">
                                                    </span>
                                                </span>
                                                <span class="box_text">
                                                    <h2 class="title">Protes Kenaikan BBM, Elite PDIP Ini Sebut Jokowi Presiden Prematur</h2>
                                                </span>
                                            </a>
                                        </article>
                                        <article>
                                            <a href="#">
                                                <span class="ratiobox box_img">
                                                    <span class="ratiobox_content lqd">
                                                        <img src="images/img1.jpg" alt="">
                                                    </span>
                                                </span>
                                                <span class="box_text">
                                                    <h2 class="title">Asus Zenfone 2 RAM 4 GB Masuk Indonesia, Harganya?</h2>
                                                </span>
                                            </a>
                                        </article>
                                    </div>

                                    <br><br>

                                    <h4 class="mb10">Media Rows Middle</h4>

                                    <hr>

                                    <div class="list media_rows middle">
                                        <article>
                                            <a href="#">
                                                <span class="ratiobox box_img">
                                                    <span class="ratiobox_content lqd">
                                                        <img src="images/img1.jpg" alt="">
                                                    </span>
                                                </span>
                                                <span class="box_text">
                                                    <h2 class="title">Saat Nurdin Halid Kesal dan Menantang Ical yang Penakut</h2>
                                                </span>
                                            </a>
                                        </article>
                                        <article>
                                            <a href="#">
                                                <span class="ratiobox box_img">
                                                    <span class="ratiobox_content lqd">
                                                        <img src="images/img1.jpg" alt="">
                                                    </span>
                                                </span>
                                                <span class="box_text">
                                                    <h2 class="title">Protes Kenaikan BBM, Elite PDIP Ini Sebut Jokowi Presiden Prematur</h2>
                                                </span>
                                            </a>
                                        </article>
                                        <article>
                                            <a href="#">
                                                <span class="ratiobox box_img">
                                                    <span class="ratiobox_content lqd">
                                                        <img src="images/img1.jpg" alt="">
                                                    </span>
                                                </span>
                                                <span class="box_text">
                                                    <h2 class="title">Asus Zenfone 2 RAM 4 GB Masuk Indonesia, Harganya?</h2>
                                                </span>
                                            </a>
                                        </article>
                                    </div>

                                </div>

                                <figure class="mb20">

                                    <h4 class="mb10">Media Rows</h4> <hr>

                                    <pre>
    <code class="language-html" data-lang="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"list media_rows"</span><span class="nt">&gt;</span>

        <span class="nt">&lt;article</span><span class="nt">&gt;</span>
            <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>
                <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"ratiobox box_img"</span><span class="nt">&gt;</span>
                    <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"ratiobox_content lqd"</span><span class="nt">&gt;</span>
                        <span class="nt">&lt;img</span> <span class="na">src=</span><span class="s">"images/img1.jpg"</span><span class="nt">&gt;</span>
                    <span class="nt">&lt;/span&gt;</span>
                <span class="nt">&lt;/span&gt;</span>
                <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"box_text"</span><span class="nt">&gt;</span>
                    <span class="nt">&lt;h2</span> <span class="na">class=</span><span class="s">"title"</span><span class="nt">&gt;</span>Mawan Sidarta<span class="nt">&lt;/h2&gt;</span>
                    Rinjani, salah satu gunung termegah dan tercantik di Indonesia
                <span class="nt">&lt;/span&gt;</span>
            <span class="nt">&lt;/a&gt;</span>
        <span class="nt">&lt;/article&gt;</span>

        <span class="nt">&lt;article</span><span class="nt">&gt;</span>
            <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>
                <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"ratiobox box_img"</span><span class="nt">&gt;</span>
                    <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"ratiobox_content lqd"</span><span class="nt">&gt;</span>
                        <span class="nt">&lt;img</span> <span class="na">src=</span><span class="s">"images/img1.jpg"</span><span class="nt">&gt;</span>
                    <span class="nt">&lt;/span&gt;</span>
                <span class="nt">&lt;/span&gt;</span>
                <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"box_text"</span><span class="nt">&gt;</span>
                    <span class="nt">&lt;h2</span> <span class="na">class=</span><span class="s">"title"</span><span class="nt">&gt;</span>Mawan Sidarta<span class="nt">&lt;/h2&gt;</span>
                    Rinjani, salah satu gunung termegah dan tercantik di Indonesia
                <span class="nt">&lt;/span&gt;</span>
            <span class="nt">&lt;/a&gt;</span>
        <span class="nt">&lt;/article&gt;</span>

        <span class="nt">&lt;article</span><span class="nt">&gt;</span>
            <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>
                <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"ratiobox box_img"</span><span class="nt">&gt;</span>
                    <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"ratiobox_content lqd"</span><span class="nt">&gt;</span>
                        <span class="nt">&lt;img</span> <span class="na">src=</span><span class="s">"images/img1.jpg"</span><span class="nt">&gt;</span>
                    <span class="nt">&lt;/span&gt;</span>
                <span class="nt">&lt;/span&gt;</span>
                <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"box_text"</span><span class="nt">&gt;</span>
                    <span class="nt">&lt;h2</span> <span class="na">class=</span><span class="s">"title"</span><span class="nt">&gt;</span>Mawan Sidarta<span class="nt">&lt;/h2&gt;</span>
                    Rinjani, salah satu gunung termegah dan tercantik di Indonesia
                <span class="nt">&lt;/span&gt;</span>
            <span class="nt">&lt;/a&gt;</span>
        <span class="nt">&lt;/article&gt;</span>

    <span class="nt">&lt;/div&gt;</span>
    </code></pre>

    <br><br>

    <h4 class="mb10">Media Rows Middle</h4> <hr>

                                    <pre>
    <code class="language-html" data-lang="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"list media_rows middle"</span><span class="nt">&gt;</span>

        <span class="nt">&lt;article</span><span class="nt">&gt;</span>
            <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>
                <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"ratiobox box_img"</span><span class="nt">&gt;</span>
                    <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"ratiobox_content lqd"</span><span class="nt">&gt;</span>
                        <span class="nt">&lt;img</span> <span class="na">src=</span><span class="s">"images/img1.jpg"</span><span class="nt">&gt;</span>
                    <span class="nt">&lt;/span&gt;</span>
                <span class="nt">&lt;/span&gt;</span>
                <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"box_text"</span><span class="nt">&gt;</span>
                    <span class="nt">&lt;h2</span> <span class="na">class=</span><span class="s">"title"</span><span class="nt">&gt;</span>Mawan Sidarta<span class="nt">&lt;/h2&gt;</span>
                    Rinjani, salah satu gunung termegah dan tercantik di Indonesia
                <span class="nt">&lt;/span&gt;</span>
            <span class="nt">&lt;/a&gt;</span>
        <span class="nt">&lt;/article&gt;</span>

        <span class="nt">&lt;article</span><span class="nt">&gt;</span>
            <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>
                <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"ratiobox box_img"</span><span class="nt">&gt;</span>
                    <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"ratiobox_content lqd"</span><span class="nt">&gt;</span>
                        <span class="nt">&lt;img</span> <span class="na">src=</span><span class="s">"images/img1.jpg"</span><span class="nt">&gt;</span>
                    <span class="nt">&lt;/span&gt;</span>
                <span class="nt">&lt;/span&gt;</span>
                <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"box_text"</span><span class="nt">&gt;</span>
                    <span class="nt">&lt;h2</span> <span class="na">class=</span><span class="s">"title"</span><span class="nt">&gt;</span>Mawan Sidarta<span class="nt">&lt;/h2&gt;</span>
                    Rinjani, salah satu gunung termegah dan tercantik di Indonesia
                <span class="nt">&lt;/span&gt;</span>
            <span class="nt">&lt;/a&gt;</span>
        <span class="nt">&lt;/article&gt;</span>

        <span class="nt">&lt;article</span><span class="nt">&gt;</span>
            <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>
                <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"ratiobox box_img"</span><span class="nt">&gt;</span>
                    <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"ratiobox_content lqd"</span><span class="nt">&gt;</span>
                        <span class="nt">&lt;img</span> <span class="na">src=</span><span class="s">"images/img1.jpg"</span><span class="nt">&gt;</span>
                    <span class="nt">&lt;/span&gt;</span>
                <span class="nt">&lt;/span&gt;</span>
                <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"box_text"</span><span class="nt">&gt;</span>
                    <span class="nt">&lt;h2</span> <span class="na">class=</span><span class="s">"title"</span><span class="nt">&gt;</span>Mawan Sidarta<span class="nt">&lt;/h2&gt;</span>
                    Rinjani, salah satu gunung termegah dan tercantik di Indonesia
                <span class="nt">&lt;/span&gt;</span>
            <span class="nt">&lt;/a&gt;</span>
        <span class="nt">&lt;/article&gt;</span>

    <span class="nt">&lt;/div&gt;</span>
    </code></pre>

                                </figure>

                                <h4 class="mb10">Media Kolom ~> Gunakan grid system + class .cols</h4>

                                <div class="box highlight pd15 mb20">

                                    <div class="grid_row cols">

                                        <article class="col_sm_6 col_md_3">
                                            <a href="#">
                                                <span class="ratiobox ratio_16_9 mb10">
                                                    <span class="ratiobox_content lqd">
                                                        <img src="images/img1.jpg" alt="">
                                                    </span>
                                                </span>
                                                <h2 class="title">Asus Zenfone 2 RAM 4 GB Masuk Indonesia, Harganya?</h2>
                                            </a>
                                        </article>

                                        <article class="col_sm_6 col_md_3">
                                            <a href="#">
                                                <span class="ratiobox ratio_16_9 mb10">
                                                    <span class="ratiobox_content lqd">
                                                        <img src="images/img1.jpg" alt="">
                                                    </span>
                                                </span>
                                                <h2 class="title">Protes Kenaikan BBM, Elite PDIP Ini Sebut Jokowi Presiden Prematur</h2>
                                            </a>
                                        </article>

                                        <article class="col_sm_6 col_md_3">
                                            <a href="#">
                                                <span class="ratiobox ratio_16_9 mb10">
                                                    <span class="ratiobox_content lqd">
                                                        <img src="images/img1.jpg" alt="">
                                                    </span>
                                                </span>
                                                <h2 class="title">Saat Nurdin Halid Kesal dan Menantang Ical yang Penakut</h2>
                                            </a>
                                        </article>

                                        <article class="col_sm_6 col_md_3">
                                            <a href="#">
                                                <span class="ratiobox ratio_16_9 mb10">
                                                    <span class="ratiobox_content lqd">
                                                        <img src="images/img1.jpg" alt="">
                                                    </span>
                                                </span>
                                                <h2 class="title">Asus Zenfone 2 RAM 4 GB Masuk Indonesia, Harganya?</h2>
                                            </a>
                                        </article>

                                    </div>

                                    <br><br>

                                </div>

                                <figure class="mb20">
                                    <pre>
    <code class="language-html" data-lang="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_row cols"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;article</span> <span class="na">class=</span><span class="s">"col_sm_6 col_md_3"</span><span class="nt">&gt;</span>
            <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>
                <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"ratiobox ratio_16_9 mb10"</span><span class="nt">&gt;</span>
                    <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"ratiobox_content lqd"</span><span class="nt">&gt;</span>
                        <span class="nt">&lt;img</span> <span class="na">src=</span><span class="s">"images/img1.jpg"</span><span class="nt">&gt;</span>
                    <span class="nt">&lt;/span&gt;</span>
                <span class="nt">&lt;/span&gt;</span>
                <span class="nt">&lt;h2</span> <span class="na">class=</span><span class="s">"title"</span><span class="nt">&gt;</span>Asus Zenfone 2 RAM 4 GB Masuk Indonesia, Harganya?<span class="nt">&lt;/h2&gt;</span>
            <span class="nt">&lt;/a&gt;</span>
        <span class="nt">&lt;/article&gt;</span>
        <span class="nt">&lt;article</span> <span class="na">class=</span><span class="s">"col_sm_6 col_md_3"</span><span class="nt">&gt;</span>
            <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>
                <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"ratiobox ratio_16_9 mb10"</span><span class="nt">&gt;</span>
                    <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"ratiobox_content lqd"</span><span class="nt">&gt;</span>
                        <span class="nt">&lt;img</span> <span class="na">src=</span><span class="s">"images/img1.jpg"</span><span class="nt">&gt;</span>
                    <span class="nt">&lt;/span&gt;</span>
                <span class="nt">&lt;/span&gt;</span>
                <span class="nt">&lt;h2</span> <span class="na">class=</span><span class="s">"title"</span><span class="nt">&gt;</span>Protes Kenaikan BBM, Elite PDIP Ini Sebut Jokowi Presiden Prematur<span class="nt">&lt;/h2&gt;</span>
            <span class="nt">&lt;/a&gt;</span>
        <span class="nt">&lt;/article&gt;</span>
        <span class="nt">&lt;article</span> <span class="na">class=</span><span class="s">"col_sm_6 col_md_3"</span><span class="nt">&gt;</span>
            <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>
                <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"ratiobox ratio_16_9 mb10"</span><span class="nt">&gt;</span>
                    <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"ratiobox_content lqd"</span><span class="nt">&gt;</span>
                        <span class="nt">&lt;img</span> <span class="na">src=</span><span class="s">"images/img1.jpg"</span><span class="nt">&gt;</span>
                    <span class="nt">&lt;/span&gt;</span>
                <span class="nt">&lt;/span&gt;</span>
                <span class="nt">&lt;h2</span> <span class="na">class=</span><span class="s">"title"</span><span class="nt">&gt;</span>Saat Nurdin Halid Kesal dan Menantang Ical yang Penakut<span class="nt">&lt;/h2&gt;</span>
            <span class="nt">&lt;/a&gt;</span>
        <span class="nt">&lt;/article&gt;</span>
        <span class="nt">&lt;article</span> <span class="na">class=</span><span class="s">"col_sm_6 col_md_3"</span><span class="nt">&gt;</span>
            <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>
                <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"ratiobox ratio_16_9 mb10"</span><span class="nt">&gt;</span>
                    <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"ratiobox_content lqd"</span><span class="nt">&gt;</span>
                        <span class="nt">&lt;img</span> <span class="na">src=</span><span class="s">"images/img1.jpg"</span><span class="nt">&gt;</span>
                    <span class="nt">&lt;/span&gt;</span>
                <span class="nt">&lt;/span&gt;</span>
                <span class="nt">&lt;h2</span> <span class="na">class=</span><span class="s">"title"</span><span class="nt">&gt;</span>Asus Zenfone 2 RAM 4 GB Masuk Indonesia, Harganya?<span class="nt">&lt;/h2&gt;</span>
            <span class="nt">&lt;/a&gt;</span>
        <span class="nt">&lt;/article&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
    </code></pre>

                                </figure>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        <footer id="footer">
            SOURCE : <a href="https://bitbucket.org/fsetiady/framework/src/master/" target="new">https://bitbucket.org/fsetiady/framework/src/master/</a>
        </footer>

        <?php include "includes/js.php"; ?>

        <script type="text/javascript">

            $(function () {

                $window = $(window);

                $window.scroll(function() {
                    var windowpos = $window.scrollTop();
                    var navpos = $window.height() - 64;
                    var top = $(this).scrollTop() + 64;

                    // console.log(windowpos);
                    // console.log($window.height());
        
                    $(".section").each(function(i){
                        var this_top = $(this).offset().top;
                        var height = $(this).height();
                        var this_bottom = this_top + height;
                        var percent = 0;
                        percent = ((top - this_top) / (height - 1)) * 100;
                        
                        if (percent > 100 || percent < 0) {
                            $("#menu li a:eq("+i+")").removeClass("active");
                        }
                        else {
                            $("#menu li a:eq("+i+")").addClass("active");
                        }
                    });     
                });

                // Smooth Scroll Links
                $("#menu li a").click(function (e){
                    e.preventDefault();
                    var hash = this.hash.substr(1);
                    // console.log(hash);
                    $("html, body").animate({
                    scrollTop: $("#"+ hash).offset().top - 63
                    }, 200);
                });

                //IMAGE LIQUID
                $(".lqd").imgLiquid();

            });

        </script>

    </body>

</html>