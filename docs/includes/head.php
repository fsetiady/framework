<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale = 1.0, user-scalable = no, width=device-width, height=device-height, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <link type="image/x-icon" rel="shortcut icon" href="images/favicon.ico">

    <title>FRAMEWORK</title>

    <!-- Core CSS -->
    <link href="css/style.css" rel="stylesheet">

    <!-- Custom styles for docs -->
    <link href="css/docs.css" rel="stylesheet">

    <script src="js/jquery.min.js"></script>

</head>